<?php
 
class Penduduk_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get penduduk by nik
     */
    function get_penduduk($nik)
    {
        return $this->db->get_where('penduduk',array('nik'=>$nik))->row_array();
    }
        
    /*
     * Get all penduduk
     */
    function get_all_penduduk()
    {
        $this->db->order_by('nik', 'desc');
        return $this->db->get('penduduk')->result_array();
    }
        
    /*
     * function to add new penduduk
     */
    function add_penduduk($params)
    {
        $this->db->insert('penduduk',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update penduduk
     */
    function update_penduduk($nik,$params)
    {
        $this->db->where('nik',$nik);
        return $this->db->update('penduduk',$params);
    }
    
    /*
     * function to delete penduduk
     */
    function delete_penduduk($nik)
    {
        return $this->db->delete('penduduk',array('nik'=>$nik));
    }
}
