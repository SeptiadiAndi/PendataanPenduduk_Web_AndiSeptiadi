<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_users()
	{
		$query = $this->db->get('user');
        return $query->result_array();
	}

	public function login($username, $password)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where(['username'=>$username, 'password'=>$password]);
		$result = $this->db->get('');
		if ($result->num_rows() > 0)
		{
			echo "sukses";
			return true;
		} 
		else
		{
			return false;
		}
	}

	public function insert_user()
		{
			$this->load->helper('url');

			$hobbies = implode(", ", $this->input->post('hobi'));

			$data = array(
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'email' => $this->input->post('email'),
				'nama' => $this->input->post('nama'),
				'nim' => $this->input->post('nim'),
				'alamat' => $this->input->post('alamat'),
				'kota' => $this->input->post('kota'),
				'j_kel' => $this->input->post('jenis_kelamin'),
				'hobi' => $hobbies,
				'deskripsi' => $this->input->post('deskripsi')
			);

			return $this->db->insert('user', $data);
		}
}
?>