<?php

// Code By Andi Septiadi - 1116104005

defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library('form_validation');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo $this->session->userdata('username') ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css">
		<style type="text/css">
			body {
				background-color: white;
				margin: auto;
			}

			.containerTable {
				width: 100%;
			    height: 100%;
			    position: absolute;
			}

			.sideTable {
				width: 100%;
			}

			.dataTable {
				width: 95%;
				margin: 8px;
			}

			.menuTd {
				width: 100%;
				height: 50px;
				margin: 0px auto;
			}

			.side {
				width: 200px;
				text-align: center;
				background-color: #1A237E;
				height: 100%;
				vertical-align: top;
			}

			.main {
				height: 100%;
				vertical-align: top;
			}

			.menu {
				width: 200px;
				height: 30px;
				background-color: gray;
				display: inline-block;
				padding-top: 10px;
				padding-bottom: 10px;
				border: none;
			    color: white;
			    font-size: 20px;
			    cursor: pointer;
			    text-decoration: none;
			}

			.btn {
			    border: none; /* Remove borders */
			    color: white; /* Add a text color */
			    padding: 14px 28px; /* Add some padding */
			    cursor: pointer; /* Add a pointer cursor on mouse-over */
			    margin: 8px;
			    text-decoration: none;
			}

			.success {background-color: #4CAF50;} /* Green */
			.success:hover {background-color: #46a049;}

			.info {background-color: #2196F3;} /* Blue */
			.info:hover {background: #0b7dda;}

		</style>
	</head>
	<body>
		<table cellspacing="0" border="1" class="dataTable">
			<tr style="height: 50px; background-color: #1A237E; color: white; text-align: center;">
				<td width="15%">Username</td>
				<td width="15%">Email</td>
				<td width="15%">Nama</td>
				<td width="15%">NIM</td>
				<td width="15%">Kota</td>
				<td width="25%">Deskripsi</td>
			</tr>
			<?php
			foreach ($users as $user_item):
				echo "
					<tr>
						<td>$user_item[username]</td>
						<td>$user_item[email]</td>
						<td>$user_item[nama]</td>
						<td>$user_item[nim]</td>
						<td>$user_item[kota]</td>
						<td>$user_item[deskripsi]</td>
					</tr>
				";
			endforeach;
			?>
		</table>
		<center>
			<font style="font-size: 12px;">
				- &copy;2017. Andi Septiadi - 1116104005 -
			</font>
		</center>
	</body>
</html>