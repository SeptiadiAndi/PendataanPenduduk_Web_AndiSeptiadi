<?php

// Code By Andi Septiadi - 1116104005

defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library('form_validation');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo $this->session->userdata('username') ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css">
		<style type="text/css">
			body {
				background-color: white;
				margin: auto;
			}

			.containerTable {
				width: 100%;
			    height: 100%;
			    position: absolute;
			}

			.sideTable {
				width: 100%;
			}

			.dataTable {
				width: 95%;
				margin: 8px;
			}

			.menuTd {
				width: 100%;
				height: 50px;
				margin: 0px auto;
			}

			.side {
				width: 200px;
				text-align: center;
				background-color: #1A237E;
				height: 100%;
				vertical-align: top;
			}

			.main {
				height: 100%;
				vertical-align: top;
			}

			.menu {
				width: 200px;
				height: 30px;
				background-color: gray;
				display: inline-block;
				padding-top: 10px;
				padding-bottom: 10px;
				border: none;
			    color: white;
			    font-size: 20px;
			    cursor: pointer;
			    text-decoration: none;
			}

			.btn {
			    border: none; /* Remove borders */
			    color: white; /* Add a text color */
			    padding: 14px 28px; /* Add some padding */
			    cursor: pointer; /* Add a pointer cursor on mouse-over */
			    margin: 8px;
			    text-decoration: none;
			}

			.success {background-color: #4CAF50;} /* Green */
			.success:hover {background-color: #46a049;}

			.info {background-color: #2196F3;} /* Blue */
			.info:hover {background: #0b7dda;}

			.container {
				background-color: rgba(255,255,255,1);
    			display: block;
    			border-color: #ECEFF1;
				border-radius: 3px;
				border-style: solid;
				border-width: 1px;
    			position: relative;
    			padding-top: 8px;
    			padding-bottom: 8px;
    			padding-left: 38px;
    			margin-bottom: 8px;
    			cursor: pointer;
    			font-size: 18px;
    			color: #424242;
    			-webkit-user-select: none;
    			-moz-user-select: none;
    			-ms-user-select: none;
    			user-select: none;
    			width: 65%;
			}

			.container input {
    			position: absolute;
    			opacity: 0;
			}

			.checkmark {
    			position: absolute;
    			top: 0;
    			left: 0;
   				height: 20px;
    			width: 20px;
    			margin: 8px 8px;
    			background-color: #BDBDBD;
			}

			.container:hover input ~ .checkmark {
    			background-color: rgba(255,255,255,0.75);
			}

			.container input:checked ~ .checkmark {
			    			background-color: #263238;
			}

			.checkmark:after {
			    content: "";
			    position: absolute;
			    display: none;
			}

			.container input:checked ~ .checkmark:after {
			    display: block;
			}

			.container .checkmark:after {
			    left: 6px;
			    top: 2px;
			    width: 5px;
			    height: 10px;
			    border: solid white;
			    border-width: 0 3px 3px 0;
			    -webkit-transform: rotate(45deg);
			    -ms-transform: rotate(45deg);
			    transform: rotate(45deg);
			}

			.radio {
			    position: absolute;
			    top: 0;
			    left: 0;
			    height: 20px;
			    width: 20px;
			    margin: 8px 8px;
			    background-color: #BDBDBD;
			    border-radius: 10px;
			}

			.container:hover input ~ .radio {
			    background-color: rgba(255,255,255,0.75);
			}

			.container input:checked ~ .radio {
			    background-color: #263238;
			}

			.radio:after {
			    content: "";
			    position: absolute;
			    display: none;
			}

			.container input:checked ~ .radio:after {
			    display: block;
			}

			.container .radio:after {
			    left: 6px;
			    top: 2px;
			    width: 5px;
			    height: 10px;
			    border: solid white;
			    border-width: 0 3px 3px 0;
			    -webkit-transform: rotate(45deg);
			    -ms-transform: rotate(45deg);
			    transform: rotate(45deg);
			}

			.table {
				border: 4px solid rgba(255,255,255,0.5);
				border-radius: 3px;
				background-color: #1A237E;
				margin-top: 10px;
				width: 800px;
			}

			.input {
    			padding: 8px 8px;
    			box-sizing: border-box;
    			border: 1px solid #ccc;
    			-webkit-transition: 0.5s;
    			transition: 0.5s;
    			outline: none;
    			margin-top: 8px;
    			width: 95%;
			}

			.submit {
				border: none;
    			color: #212121;
    			padding: 14px 28px;
    			font-size: 16px;
    			cursor: pointer;
    			margin-top: 8px;
    			width: 95%;
    			background-color: #2196F3;
    			font-weight: 500;
			}

			.submit:hover {
				background-color: #1565C0;
				color: white;
			}

			.input:focus {
    			border: 3px solid #555;
			}

			.login {
    			color: white;
			}

			.label {
				color: white;
				margin-left: 18px;
			}
			div.alert {
				width: 100%;
				background-color: #da190b;
				color: white;
				padding-top: 20px;
				padding-bottom: 20px;
			}

			.detail {
				color: gray;
				margin-left: 18px;
				margin-right: 18px;
			}

		</style>
	</head>
	<body>
		<?php
			if (isset($error)) {
				echo "
					<div class='alert'>
						$error
					</div>
				";
			}
		?>
		<?php echo validation_errors(); ?>
			<table cellspacing="0" class="containerTable">
				<tr>
					<td class="side">
						<table cellspacing="0" class="sideTable">
							<tr>
								<td>
									<a href="<?=base_url()?>index.php/penduduk" class="menu info">Users</a>
								</td>
							</tr>
							<tr>
								<td class="menuTd">
									<a href="<?=base_url()?>index.php/penduduk/user_logout" class="menu info">Logout</a>
								</td>
							</tr>
						</table>
					</td>
					<td class="main">
						<h1 style="margin: 8px;">Selamat Datang <?php echo $this->session->userdata('username') ?></h1>
						<hr>
						<br>
						<!-- Start of list user -->
						<?php  
						if ($_view == 'add') {
						?>
							<?php echo form_open_multipart('penduduk/add'); ?>
							<table class="table">
								<tr>
									<td><p class="label">NIK</p></td>
									<td><input type="text" name="nik" class="input" placeholder="NIK" maxlength="16" minlength="16" value="<?php echo $this->input->post('nik'); ?>" required /></td>
								</tr>
								<tr>
									<td><p class="label">Nama</p></td>
									<td><input type="text" name="nama" class="input" placeholder="Nama" value="<?php echo $this->input->post('nama'); ?>" required /></td>
								</tr>
								<tr >
									<td><p class="label">Foto</p></td>
									<td>
										<input type="file" name="foto" style="color: white; height: 30px;" required />
									</td>
								</tr>
								<tr>
									<td><p class="label">Alamat</p></td>
									<td><textarea name="alamat" rows="5" class="input" cols="25"><?php echo $this->input->post('alamat'); ?></textarea></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: center;">
										<input type="submit" class="submit" name="kirim" value="Kirim">
									</td>
								</tr>
							</table>

						<?php echo form_close(); ?>
						<?php
						} else if ($_view == 'edit') {
						?>
						<?php echo form_open('penduduk/edit/'.$penduduk['nik']); ?>

						<table class="table">
								<tr>
									<td><p class="label">Nama</p></td>
									<td><input type="text" name="nama" class="input" placeholder="Nama" value="<?php echo ($this->input->post('nama') ? $this->input->post('nama') : $penduduk['nama']); ?>" required /></td>
								</tr>
								<tr>
									<td><p class="label">Alamat</p></td>
									<td><textarea name="alamat" rows="5" class="input" cols="25"><?php echo ($this->input->post('alamat') ? $this->input->post('alamat') : $penduduk['alamat']); ?></textarea></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: center;">
										<input type="submit" class="submit" name="kirim" value="Kirim">
									</td>
								</tr>
							</table>
						
					<?php echo form_close(); ?>
						<?php
						} else if ($_view == 'detail') {
						?>

						<!-- Halaman Detail Penduduk -->

						<br><br>
						<table cellspacing="0" class="dataTable">
							<tr>
								<td colspan="2">
									<center>
										<img src="<?php echo base_url(); ?>uploads/<?php echo($penduduk['foto'])?>" style="background-color: white; width: 160px; height: auto; margin-top: 8px;" />
									</center>
								</td>
							</tr>
							<tr>
								<td width="200"><p class="detail">NIK</p></td>
								<td><p class="detail">: <?php echo $penduduk['nik']; ?></p></td>
							</tr>
							<tr>
								<td><p class="detail">Nama</p></td>
								<td><p class="detail">: <?php echo $penduduk['nama']; ?></p></td>
							</tr>
							<tr>
								<td><p class="detail">Alamat</p></td>
								<td><p class="detail">: <?php echo $penduduk['alamat']; ?></p></td>
							</tr>
							<tr>
								<td><p class="detail">ID Kota</p></td>
								<td><p class="detail">: <?php echo $penduduk['kota']; ?></p></td>
							</tr>
							<tr>
								<td><p class="detail">Jenis Kelamin</p></td>
								<td><p class="detail">: <?php echo $penduduk['j_kel']; ?></p></td>
							</tr>
							<tr>
								<td><p class="detail">Tanggal Lahir</p></td>
								<td><p class="detail">: <?php echo $penduduk['tanggal_lahir']; ?></p></td>
							</tr>
							<tr>
								<td><p class="detail">Provinsi</p></td>
								<td><p class="detail">: <?php echo $penduduk['kode_provinsi']; ?></p></td>
							</tr>
						</table>

						<?php
						} else {
						?>
							<a href="<?php echo site_url('penduduk/add'); ?>" class="btn success">Tambah</a>
							<br>
							<br>
							<table cellspacing="0" border="1" class="dataTable">
								<tr style="height: 50px; background-color: #1A237E; color: white; text-align: center;">
									<th width="10">Nik</th>
									<th width="20%">Nama</th>
									<th width="10">Kota</th>
									<th width="10">J Kel</th>
									<th width="10">Tanggal Lahir</th>
									<th width="10">Kode Provinsi</th>
									<th width="10">Foto</th>
									<th width="10">Alamat</th>
									<th width="10">Actions</th>
								</tr>
								<?php foreach($penduduk as $p){ ?>
							    <tr>
									<td><?php echo $p['nik']; ?></td>
									<td><?php echo $p['nama']; ?></td>
									<td><?php echo $p['kota']; ?></td>
									<td><?php echo $p['j_kel']; ?></td>
									<td><?php echo $p['tanggal_lahir']; ?></td>
									<td><?php echo $p['kode_provinsi']; ?></td>
									<td><?php echo $p['foto']; ?></td>
									<td><?php echo $p['alamat']; ?></td>
									<td>
							            <a href="<?php echo site_url('penduduk/edit/'.$p['nik']); ?>">Edit</a> | 
							            <a href="<?php echo site_url('penduduk/remove/'.$p['nik']); ?>">Delete</a> | 
							            <a href="<?php echo site_url('penduduk/detail/'.$p['nik']); ?>">Detail</a>
							        </td>
							    </tr>
							    <?php } ?>
							</table>
						<?php
						}
						?>
						<!-- end of list user -->
						<center>
							<font style="font-size: 12px;">
								- &copy;2017. Andi Septiadi - 1116104005 -
							</font>
						</center>
					</td>
				</tr>
			</table>
	</body>
</html>