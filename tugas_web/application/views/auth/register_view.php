<?php
	// Code By Andi Septiadi - 1116104005

	defined('BASEPATH') OR exit('No direct script access allowed');

	$this->load->library('form_validation');
?>
<html>
	<head>
		<title>Register</title>
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css"> -->
		<style type="text/css">
			body {
				background-color: white;
				margin: auto;
			}

			.container {
				background-color: rgba(255,255,255,1);
    			display: block;
    			border-color: #ECEFF1;
				border-radius: 3px;
				border-style: solid;
				border-width: 1px;
    			position: relative;
    			padding-top: 8px;
    			padding-bottom: 8px;
    			padding-left: 38px;
    			margin-bottom: 8px;
    			cursor: pointer;
    			font-size: 18px;
    			color: #424242;
    			-webkit-user-select: none;
    			-moz-user-select: none;
    			-ms-user-select: none;
    			user-select: none;
    			width: 85%;
			}

			.container input {
    			position: absolute;
    			opacity: 0;
			}

			.checkmark {
    			position: absolute;
    			top: 0;
    			left: 0;
   				height: 20px;
    			width: 20px;
    			margin: 8px 8px;
    			background-color: #BDBDBD;
			}

			.container:hover input ~ .checkmark {
    			background-color: rgba(255,255,255,0.75);
			}

			.container input:checked ~ .checkmark {
			    			background-color: #263238;
			}

			.checkmark:after {
			    content: "";
			    position: absolute;
			    display: none;
			}

			.container input:checked ~ .checkmark:after {
			    display: block;
			}

			.container .checkmark:after {
			    left: 6px;
			    top: 2px;
			    width: 5px;
			    height: 10px;
			    border: solid white;
			    border-width: 0 3px 3px 0;
			    -webkit-transform: rotate(45deg);
			    -ms-transform: rotate(45deg);
			    transform: rotate(45deg);
			}

			.radio {
			    position: absolute;
			    top: 0;
			    left: 0;
			    height: 20px;
			    width: 20px;
			    margin: 8px 8px;
			    background-color: #BDBDBD;
			    border-radius: 10px;
			}

			.container:hover input ~ .radio {
			    background-color: rgba(255,255,255,0.75);
			}

			.container input:checked ~ .radio {
			    background-color: #263238;
			}

			.radio:after {
			    content: "";
			    position: absolute;
			    display: none;
			}

			.container input:checked ~ .radio:after {
			    display: block;
			}

			.container .radio:after {
			    left: 6px;
			    top: 2px;
			    width: 5px;
			    height: 10px;
			    border: solid white;
			    border-width: 0 3px 3px 0;
			    -webkit-transform: rotate(45deg);
			    -ms-transform: rotate(45deg);
			    transform: rotate(45deg);
			}

			.table {
				border: 4px solid rgba(255,255,255,0.5);
				border-radius: 3px;
				background-color: #1A237E;
				margin-top: 10px;
				width: 640px;
			}

			.input {
    			padding: 8px 8px;
    			box-sizing: border-box;
    			border: 1px solid #ccc;
    			-webkit-transition: 0.5s;
    			transition: 0.5s;
    			outline: none;
    			margin-top: 8px;
    			width: 95%;
			}

			.submit {
				border: none;
    			color: #212121;
    			padding: 14px 28px;
    			font-size: 16px;
    			cursor: pointer;
    			margin-top: 8px;
    			width: 95%;
    			background-color: #2196F3;
    			font-weight: 500;
			}

			.submit:hover {
				background-color: #1565C0;
				color: white;
			}

			.input:focus {
    			border: 3px solid #555;
			}

			.login {
    			color: white;
			}

			.label {
				color: white;
				margin-left: 18px;
			}

		</style>
	</head>
	<body>
		<center>
		<?php echo validation_errors(); ?>
		<?php echo form_open('register/user_register'); ?>
			<table class="table">
				<tr>
					<td width="175"><p class="label">Username</p></td>
					<td><input type="text" name="username" class="input" placeholder="Username" required></td>
				</tr>
				<tr>
					<td><p class="label">Password</p></td>
					<td><input type="password" name="password" class="input" placeholder="Password" required></td>
				</tr>
				<tr>
					<td><p class="label">Re-type Password</p></td>
					<td><input type="password" name="retype" class="input" placeholder="Re-type Password" required></td>
				</tr>
				<tr>
					<td><p class="label">E-mail</p></td>
					<td><input type="email" name="email" class="input" placeholder="E-mail" required></td>
				</tr>
				<tr>
					<td><p class="label">Nama</p></td>
					<td><input type="text" name="nama" class="input" placeholder="Nama" required></td>
				</tr>
				<tr>
					<td><p class="label">NIM</p></td>
					<td><input type="text" name="nim" class="input" placeholder="NIM" required></td>
				</tr>
				<tr>
					<td><p class="label">Alamat</p></td>
					<td><input type="text" name="alamat" class="input" placeholder="Alamat" required></td>
				</tr>
				<tr >
					<td><p class="label">Kota Asal</p></td>
					<td>
						<select name="kota" class="input" required>
							<option value=""></option>
							<option value="Bandung">Bandung</option>
							<option value="Bandung">Jakarta</option>
							<option value="Bandung">Cimahi</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><p class="label">Jenis Kelamin</p></td>
					<td>
						<label class="container" style="margin-top: 18px;">Pria
  							<input type="radio" name="jenis_kelamin" value="l" checked="checked">
  							<span class="radio"></span>
						</label>
						<label class="container" style="margin-bottom: 18px;">Wanita
  							<input type="radio" name="jenis_kelamin" value="p">
  							<span class="radio"></span>
						</label>
					</td>
				</tr>
				<tr>
					<td><p class="label">Hobi</p></td>
					<td>
						<label class="container">Coding
  							<input type="checkbox" name="hobi[]" value="Coding" checked="checked">
  							<span class="checkmark"></span>
						</label>
						<label class="container">PSan
  							<input type="checkbox" name="hobi[]" value="PSan">
  							<span class="checkmark"></span>
						</label>
						<label class="container">Bilyard
  							<input type="checkbox" name="hobi[]" value="Bilyard">
  							<span class="checkmark"></span>
						</label>
					</td>
				</tr>
				<tr>
					<td><p class="label">Deskripsi Pribadi</p></td>
					<td>
						<textarea name="deskripsi" rows="5" class="input" cols="25" placeholder="Deskripsi Pribadi"></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;">
						<input type="submit" class="submit" name="kirim" value="Kirim">
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;" height="28">
						<a href="<?=base_url()?>index.php/login" class="login">Anda sudah memiliki akun? Silahkan login</a>
					</td>
				</tr>
			</table>
		</form>
		<font style="font-size: 12px;">
			- &copy;2017. Andi Septiadi - 1116104005 -
		</font>
		</center>
	</body>
</html>