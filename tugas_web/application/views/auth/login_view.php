<?php

// Code By Andi Septiadi - 1116104005

defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library('form_validation');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<title>Login</title>
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css"> -->
		<style type="text/css">
			body {
				background-color: white;
				margin: auto;
			}

			.container {
				border: 4px solid rgba(255,255,255,0.5);
				border-radius: 3px;
				background-color: #1A237E;
				margin-top: 15%;
				width: 280px;
			}

			.input {
    			padding: 8px 8px;
    			box-sizing: border-box;
    			border: 1px solid #ccc;
    			-webkit-transition: 0.5s;
    			transition: 0.5s;
    			outline: none;
    			margin-top: 8px;
    			width: 90%;
			}

			.submit {
				border: none;
    			color: #212121;
    			padding: 14px 28px;
    			font-size: 16px;
    			cursor: pointer;
    			margin-top: 8px;
    			width: 90%;
    			background-color: #2196F3;
    			font-weight: 500;
			}

			.submit:hover {
				background-color: #1565C0;
				color: white;
			}

			.input:focus {
    			border: 3px solid #555;
			}

			.login {
    			color: white;
			}

			div.alert {
				width: 100%;
				background-color: #da190b;
				color: white;
				padding-top: 20px;
				padding-bottom: 20px;
			}

		</style>
	</head>
	<body>
		<center>
			<?php
				if (isset($error)) {
					echo "
						<div class='alert'>
							$error
						</div>
					";
				}
			?>
			
		<?php echo validation_errors(); ?>
		<form action="<?=base_url()?>index.php/login/user_login" method="POST">
			<table class="container" cellspacing="0">
				<tr>
					<td style="text-align: center;">
						<input type="text" name="username" class="input" placeholder="Username" required>
					</td>
				</tr>
				<tr>
					<td style="text-align: center;">
						<input type="password" name="password" class="input" placeholder="Password" required>
					</td>
				</tr>
				<tr>
					<td style="text-align: center;">
						<input type="submit" class="submit" name="kirim" value="MASUK">
					</td>
				</tr>
				<tr>
					<td style="text-align: center;" height="28">
						<a href="<?=base_url()?>index.php/register" class="login">Daftar Baru</a>
					</td>
				</tr>
			</table>
		</form>
		<font style="font-size: 12px;">
			- &copy;2017. Andi Septiadi - 1116104005 -
		</font>
		</center>
	</body>
</html>