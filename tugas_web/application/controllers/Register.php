<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Code By Andi Septiadi - 1116104005

class Register extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url', 'html');
		$this->load->model('UserModel');

		if(!empty($this->session->userdata('username'))){
			redirect(base_url().'index.php/user');
		}
	}

	public function index()
	{
		$this->load->view('auth/register_view');
	}

	public function user_register()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		// $this->form_validation->set_rules('username','Username','required');
		// $this->form_validation->set_rules('nama','Nama','required');
		// $this->form_validation->set_rules('email','Email','trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('retype', 'Retype Password', 'matches[password]');

		if ($this->form_validation->run() === FALSE)
    	{
			$this->load->view('auth/register_view');
    	}
    	else 
    	{
			$this->UserModel->insert_user();
			$this->load->view('auth/register_view');
    	}
	}
}
?>