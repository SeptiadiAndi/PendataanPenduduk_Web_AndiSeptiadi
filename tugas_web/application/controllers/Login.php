<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Code By Andi Septiadi - 1116104005

class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url', 'html');
		$this->load->model('UserModel');

		if(!empty($this->session->userdata('username'))){
			redirect(base_url().'index.php/user');
		}
	}

	public function index()
	{
		$this->load->view('auth/login_view');
	}

	public function user_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$result = $this->UserModel->login($username, $password);
		if ($result == TRUE) 
		{
			$session_data = array(
				'username' => $username
			);
			$this->session->set_userdata($session_data);
			redirect(base_url().'index.php/penduduk');
		}
		else
		{
			$data["error"] = "ERROR! Username atau password yang anda masukan salah";
			$this->load->view('auth/login_view', $data);
		}
	}
}
?>