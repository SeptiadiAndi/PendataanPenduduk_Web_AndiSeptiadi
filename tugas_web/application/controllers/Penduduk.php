<?php
 
class Penduduk extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penduduk_model');

        if(empty($this->session->userdata('username'))){
            redirect(base_url().'index.php/login');
        }
    }

    function bulan($i) {
        $i = intval($i) - 1;
        $data = array(
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        if (isset($data[$i])) {
            return trim($data[$i]);
        }
    }  

    function kode_provinsi($i)
    {
        $i = intval($i);
        $data = array(
            11 => 'Aceh',
            12 => 'Sumatera Utara',
            13 => 'Sumatera Barat',
            14 => 'Riau',
            15 => 'Jambi',
            16 => 'Sumatera Selatan',
            17 => 'Bengkulu',
            18 => 'Lampung',
            19 => 'Kep. Bangka Belitung',
            21 => 'Kep. Riau',
            31 => 'DKI Jakarta',
            32 => 'Jawa Barat',
            33 => 'Jawa Tengah',
            34 => 'Yogyakarta',
            35 => 'Jawa Timur',
            36 => 'Banten',
            51 => 'Bali',
            52 => 'Nusa Tenggara Barat',
            53 => 'Nusa Tenggara Timur',
            61 => 'Kalimantan Barat',
            62 => 'Kalimantan Tengah',
            63 => 'Kalimantan Selatan',
            64 => 'Kalimantan Timur',
            71 => 'Sulawesi Utara',
            72 => 'Sulawesi Tengah',
            73 => 'Sulawesi Selatan',
            74 => 'Sulawesi Tenggara',
            75 => 'Gorontalo',
            76 => 'Sulawesi Barat',
            81 => 'Maluku',
            82 => 'Maluku Utara',
            91 => 'Papua Barat',
            94 => 'Papua'
        );
        if (isset($data[$i])) {
            return trim($data[$i]);
        }
    }

    /*
     * Listing of penduduk
     */
    function index()
    {
        $data['penduduk'] = $this->Penduduk_model->get_all_penduduk();
        
        $data['_view'] = 'penduduk/index';
        $this->load->view('user/penduduk_main',$data);
    }

    /*
     * Adding a new penduduk
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {
            $nik = trim($this->input->post('nik'));

            $idProvinsi = substr($nik, 0, 2);
            $idKota = substr($nik, 2, 2);
            $idKecamatan = substr($nik, 4, 2);
            $tanggalLahir = substr($nik, 6, 2);
            $bulanLahir = substr($nik, 8, 2);
            $tahunLahir = substr($nik, 10, 2);
            $idUnik = substr($nik, 12, 4);
            $gender = '';

            if (intval($tanggalLahir) > 40) {
                $tanggalLahir = intval($tanggalLahir) - 40;
                $gender = 'p';
            } else {
                $tanggalLahir = intval($tanggalLahir);
                $gender = 'l';
            }

            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            // $config['max_size']     = '100';
            // $config['max_width'] = '1024';
            // $config['max_height'] = '768';
            $config['file_name'] = $this->input->post('nik');

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('foto'))
            {
                $error = array('error' => $this->upload->display_errors());
                show_error("Gambar yang anda masukkan salah");
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
                $uploadData = $data[upload_data];
            }

            $params = array(
                'nik' => $nik,
                'nama' => $this->input->post('nama'),
                'kota' => $idKota,
                'j_kel' => $gender,
                'tanggal_lahir' => '19'.$tahunLahir.'-'.$bulanLahir.'-'.$tanggalLahir,
                'kode_provinsi' => $this->kode_provinsi($idProvinsi),
                'foto' => $uploadData[file_name],
                'alamat' => $this->input->post('alamat'),
            );
            
            $penduduk_id = $this->Penduduk_model->add_penduduk($params);
            redirect('penduduk/index');
        }
        else
        {            
            $data['_view'] = 'add';
            $this->load->view('user/penduduk_main',$data);
        }
    }  

    /*
     * Editing a penduduk
     */
    function edit($nik)
    {   
        // check if the penduduk exists before trying to edit it
        $data['penduduk'] = $this->Penduduk_model->get_penduduk($nik);
        
        if(isset($data['penduduk']['nik']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
                    'nama' => $this->input->post('nama'),
                    'alamat' => $this->input->post('alamat'),
                );

                $this->Penduduk_model->update_penduduk($nik,$params);            
                redirect('penduduk/index');
            }
            else
            {
                $data['_view'] = 'edit';
                $this->load->view('user/penduduk_main',$data);
            }
        }
        else
            show_error('The penduduk you are trying to edit does not exist.');
    } 

    /*
     * Deleting penduduk
     */
    function remove($nik)
    {
        $penduduk = $this->Penduduk_model->get_penduduk($nik);

        // check if the penduduk exists before trying to delete it
        if(isset($penduduk['nik']))
        {
            $this->Penduduk_model->delete_penduduk($nik);
            redirect('penduduk/index');
        }
        else
            show_error('The penduduk you are trying to delete does not exist.');
    }

    function detail($nik)
    {
        $data['penduduk'] = $this->Penduduk_model->get_penduduk($nik);
        
        if(isset($data['penduduk']['nik']))
        {
            $data['_view'] = 'detail';
            $this->load->view('user/penduduk_main',$data);
        }
        else
            show_error('The penduduk you are trying to see does not exist.');
    }

    public function user_logout()
    {
        unset($_SESSION['username']);

        $this->session->set_flashdata('message', 'Berhasil Logout');
        $this->session->set_flashdata('status', true);
        redirect(base_url().'index.php/login');
    }
    
}
