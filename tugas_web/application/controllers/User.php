<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Code By Andi Septiadi - 1116104005

class User extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url', 'html');
		$this->load->model('UserModel');

		if(empty($this->session->userdata('username'))){
			redirect(base_url().'index.php/login');
		}
	}

	public function index()
	{
		$data['users'] = $this->UserModel->get_users();
		$this->load->view('user/user_view', $data);
	}

	public function user_logout()
	{
		unset($_SESSION['username']);

		$this->session->set_flashdata('message', 'Berhasil Logout');
		$this->session->set_flashdata('status', true);
		redirect(base_url().'index.php/login');
	}

	public function download_user_list()
	{
		$data['users'] = $this->UserModel->get_users();
		$this->load->library('pdf');
		$this->pdf->load_view('user/list_user', $data);
	}
}
?>